<?php

namespace Drupal\adstxt\Controller;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheableResponse;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Http\Exception\CacheableNotFoundHttpException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides output ads.txt output.
 */
class AdsTxtController extends ControllerBase {

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * AdsTxt module 'adstxt.settings' configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $moduleConfig;

  /**
   * Constructs a AdsTxtController object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   Configuration object factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(ConfigFactoryInterface $config, ModuleHandlerInterface $module_handler) {
    $this->moduleConfig = $config->get('adstxt.settings');
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler')
    );
  }

  /**
   * Serves the configured ads.txt file.
   *
   * @return \Drupal\Core\Cache\CacheableResponse
   *   The ads.txt file as a response object with 'text/plain' content type.
   */
  public function build() {
    return $this->getResponse('content', 'adstxt');
  }

  /**
   * Serves the configured app-ads.txt file.
   *
   * @return \Drupal\Core\Cache\CacheableResponse
   *   The app-ads.txt file as a response object with 'text/plain' content type.
   */
  public function buildAppAds() {
    return $this->getResponse('app_content', 'app_adstxt');
  }

  /**
   * Returns the appropriate ads.txt response object.
   *
   * @param string $key
   *   The configuration key..
   * @param string $hook
   *   The relevant Drupal hook for the content.
   *
   * @return \Drupal\Core\Cache\CacheableResponse
   *   The ads.txt response object.
   */
  protected function getResponse($key, $hook) {
    $content = [];
    $content[] = $this->moduleConfig->get($key);

    // The results are dependent on the module configuration.
    $cacheable_metadata = CacheableMetadata::createFromObject($this->moduleConfig);

    // Hook other modules for adding additional lines and cache dependencies.
    if ($additions = $this->moduleHandler->invokeAll($hook, [$cacheable_metadata])) {
      $content = array_merge($content, $additions);
    }

    // Trim any extra whitespace and filter out empty strings.
    $content = array_map('trim', $content);
    $content = array_filter($content);
    $content = implode("\n", $content);

    if (empty($content)) {
      throw new CacheableNotFoundHttpException($cacheable_metadata);
    }

    $response = new CacheableResponse($content, 200, ['Content-Type' => 'text/plain']);
    $response->addCacheableDependency($cacheable_metadata);

    return $response;
  }

}
