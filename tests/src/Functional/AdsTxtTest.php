<?php

namespace Drupal\Tests\adstxt\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests functionality of configured ads.txt files.
 *
 * @group adstxt
 */
class AdsTxtTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected $profile = 'standard';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['adstxt', 'node'];

  /**
   * Checks that an administrator can view the configuration page.
   */
  public function testAdsTxtAdminAccess() {
    // Create user.
    $admin_user = $this->drupalCreateUser(['administer ads.txt']);
    $this->drupalLogin($admin_user);
    $this->drupalGet('admin/config/system/adstxt');
    $this->assertSession()->fieldExists('edit-adstxt-content');
  }

  /**
   * Checks that a non-administrative user cannot use the configuration page.
   */
  public function testAdsTxtUserNoAccess() {
    // Create user.
    $normal_user = $this->drupalCreateUser(['access content']);
    $this->drupalLogin($normal_user);
    $this->drupalGet('admin/config/system/adstxt');
    $this->assertSession()->statusCodeEquals(403);
    $this->assertSession()->fieldNotExists('edit-adstxt-content');
  }

  /**
   * Test that the ads.txt path delivers content with an appropriate header.
   */
  public function testAdsTxtPath() {
    $this->drupalGet('ads.txt');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains('greenadexchange.com, 12345, DIRECT, AEC242');
    $this->assertSession()->responseContains('blueadexchange.com, 4536, DIRECT');
    $this->assertSession()->responseContains('silverssp.com, 9675, RESELLER');
    $this->assertSession()->responseHeaderEquals('Content-Type', 'text/plain; charset=UTF-8');
  }

  /**
   * Test that the ads.txt path delivers content with an appropriate header.
   */
  public function testAppAdsTxtPath() {
    $this->drupalGet('app-ads.txt');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains('onetwothree.com, 12345, DIRECT, AEC242');
    $this->assertSession()->responseContains('fourfivesix.com, 4536, DIRECT');
    $this->assertSession()->responseContains('97whatever.com, 9675, RESELLER');
    $this->assertSession()->responseHeaderEquals('Content-Type', 'text/plain; charset=UTF-8');
  }

  /**
   * Checks that a configured ads.txt file is delivered as configured.
   */
  public function testAdsTxtConfigureAdsTxt() {
    // Create an admin user, log in and access settings form.
    $admin_user = $this->drupalCreateUser(['administer ads.txt']);
    $this->drupalLogin($admin_user);
    $this->drupalGet('admin/config/system/adstxt');

    $test_string = "# SimpleTest {$this->randomMachineName()}";
    $this->submitForm([
      'adstxt_content' => $test_string,
    ], t('Save configuration'));

    $this->drupalLogout();
    $this->drupalGet('ads.txt');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseHeaderEquals('Content-Type', 'text/plain; charset=UTF-8');
    $content = $this->getSession()->getPage()->getContent();
    self::assertEquals($test_string, $content, sprintf('Test string [%s] is displayed in the configured ads.txt file [%s].', $test_string, $content));
  }

  /**
   * Tests that third party hooks are applied.
   */
  public function testAdsTextHooks() {
    self::assertTrue(
      $this->container->get('module_installer')
        ->install(['adstxt_cacheable_test', 'adstxt_test']),
      'Should install third party modules.'
    );
    $this->resetAll();

    // Create an admin user, log in and access settings form.
    $admin_user = $this->drupalCreateUser(['administer ads.txt']);
    $this->drupalLogin($admin_user);
    $this->drupalGet('admin/config/system/adstxt');

    $this->submitForm([
      'adstxt_content' => "# ads.txt 1\r\n# ads.txt 2\r# ads.txt 3\n# ads.txt 4",
      'app_adstxt_content' => "# app-ads.txt 1\r\n# app-ads.txt 2\r# app-ads.txt 3\n# app-ads.txt 4",
    ], t('Save configuration'));
    $this->drupalLogout();

    // Test that the line breaks are normalized and the custom rules included.
    $this->drupalGet('ads.txt');
    $expected = "# ads.txt 1\n# ads.txt 2\n# ads.txt 3\n# ads.txt 4\n# adstxt_cacheable_test ads.txt comment\n# adstxt_test ads.txt comment";
    self::assertEquals($expected, $this->getSession()->getPage()->getContent());
    $this->drupalGet('app-ads.txt');
    $expected = "# app-ads.txt 1\n# app-ads.txt 2\n# app-ads.txt 3\n# app-ads.txt 4\n# adstxt_cacheable_test app-ads.txt comment\n# adstxt_test app-ads.txt comment";
    self::assertEquals($expected, $this->getSession()->getPage()->getContent());
  }

  /**
   * Tests that ads.txt responses are appropriately cached.
   */
  public function testAdsTextCacheability() {
    self::assertTrue(
      $this->container->get('module_installer')
        ->install(['adstxt_cacheable_test', 'adstxt_test', 'page_cache']),
      'Should install third party modules.'
    );
    $this->resetAll();

    // Should miss the cache initially.
    $this->drupalGet('ads.txt');
    $this->assertSession()->responseHeaderEquals('X-Drupal-Cache', 'MISS');
    $this->drupalGet('app-ads.txt');
    $this->assertSession()->responseHeaderEquals('X-Drupal-Cache', 'MISS');

    // Should now hit the cache.
    $this->drupalGet('ads.txt');
    $this->assertSession()->responseHeaderEquals('X-Drupal-Cache', 'HIT');
    $this->drupalGet('app-ads.txt');
    $this->assertSession()->responseHeaderEquals('X-Drupal-Cache', 'HIT');

    // Create an admin user, log in and access settings form.
    $admin_user = $this->drupalCreateUser(['administer ads.txt']);
    $this->drupalLogin($admin_user);
    $this->drupalGet('admin/config/system/adstxt');
    $this->submitForm([
      'adstxt_content' => '# ads.txt content',
      'app_adstxt_content' => '# app-ads.txt content',
    ], t('Save configuration'));
    $this->drupalLogout();

    // Should now miss the cached version following the form config save.
    $this->drupalGet('ads.txt');
    $this->assertSession()->responseHeaderEquals('X-Drupal-Cache', 'MISS');
    $this->drupalGet('app-ads.txt');
    $this->assertSession()->responseHeaderEquals('X-Drupal-Cache', 'MISS');

    // Should keep hitting the cached version of the text files.
    for ($i = 0; $i < 3; $i++) {
      $this->drupalGet('ads.txt');
      $this->assertSession()->responseHeaderEquals('X-Drupal-Cache', 'HIT');
      $this->drupalGet('app-ads.txt');
      $this->assertSession()->responseHeaderEquals('X-Drupal-Cache', 'HIT');
    }

    // Invalidate the custom tags specified by the adstxt_cacheable_test module.
    /** @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface $invalidator */
    $invalidator = $this->container->get('cache_tags.invalidator');

    // Clear the ads.txt specific cache tags.
    $invalidator->invalidateTags(['adstxt.test_tag']);
    // Should now MISS on ads.txt, but still HIT on app-ads.txt.
    $this->drupalGet('ads.txt');
    $this->assertSession()->responseHeaderEquals('X-Drupal-Cache', 'MISS');
    $this->drupalGet('app-ads.txt');
    $this->assertSession()->responseHeaderEquals('X-Drupal-Cache', 'HIT');

    // Clear the app-ads.txt specific cache tags.
    $invalidator->invalidateTags(['adstxt.app_test_tag']);
    // Should now HIT on ads.txt, but MISS on app-ads.txt.
    $this->drupalGet('ads.txt');
    $this->assertSession()->responseHeaderEquals('X-Drupal-Cache', 'HIT');
    $this->drupalGet('app-ads.txt');
    $this->assertSession()->responseHeaderEquals('X-Drupal-Cache', 'MISS');
  }

}
